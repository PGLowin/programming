Marco Falcao is a former professor, former Chief Architect in finance, now speaker, consultant, father, diabetic, and Microsoft employee. 
While it might be causing us to slack a bit on our actual computer skills, it also advances us because we don't have to constantly reinvent the wheel as we did in the old days. 
He is a failed stand-up comic, a cornrower, and a book author. If you never found a given library already out there somewhere what other choice did you have
http://freegiftcodes.eu